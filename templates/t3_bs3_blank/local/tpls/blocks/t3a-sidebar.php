<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// get params
$sitename = $this->params->get('sitename');
$slogan = $this->params->get('slogan', '');
$logotype = $this->params->get('logotype', 'text');
$logoimage = $logotype == 'image' ? $this->params->get('logoimage') : '';

if (!$sitename) {
    $sitename = JFactory::getConfig()->get('sitename');
}

$user = JFactory::getUser();

?>

<?php if (!$user->guest) : ?>
    <aside class='t3a-aside col-lg-2 col-md-3 col-sm-12 col-xs-12'>
        <div class='t3a-sidebar-wrapper'>
            <header id='t3a-header' class='t3a-header'>
                <div class='row'>
                    <?php if ($logoimage) { ?>
                        <div class='col-sm-3 hidden-xs'>
                            <a href="<?php echo JUri::base() ?>" title="<?php echo strip_tags($sitename) ?>">
                                <img src='<?php echo JUri::base() . $logoimage ?>' class='img-responsive' />
                            </a>
                        </div>
                    <?php } ?>
                    <div class='<?php echo ($logoimage ? "col-sm-6" : "col-sm-10") ?> col-xs-10'>
                        <a href="<?php echo JUri::base() ?>" title="<?php echo strip_tags($sitename) ?>">
                            <span><?php echo $sitename ?></span>
                        </a>
                    </div>        
                    <div class="col-xs-2 col-sm-3 visible-xs visible-sm text-right">
                        <span class="fa fa-times toggle-menu"></span>
                    </div>
                </div>
            </header>
            <div class='user-profile row'>
                <div class='col-sm-2 text-left'>
                    <i class='fa fa-user fa-2x img-reponsive'></i>
                </div>
                <div class='col-sm-6'>
                    <?php echo sprintf(JText::_("Ciao, %s!"), $user->name) ?>
                </div>
                <div class='col-sm-4 text-right'>
                    <i class='fa fa-bell notification inactive'></i>
                </div>
            </div>
            <nav class='t3a-navbar'>
                <div class='row'>
                    <jdoc:include type="modules" name="sidebar-menu" style="T3Xhtml" />
                </div>
            </nav>
            <footer class='t3a-footer'>
                <ul class='row'>
                    <li class='col-sm-9 col-xs-9 powered-by'><a href='http://www.swypelab.com/' targer='_blank'>Powered by SwypeLab SRL <img src='http://www.swypelab.com/firma/swypelab.png'></a></li>
                    <li class='col-sm-3 col-xs-3 center'><a href='<?php echo JRoute::_("index.php?option=com_users&view=login&layout=logout&task=user.menulogout") ?>'><i class='fa fa-power-off'></i></a></li>
                </ul>
            </footer>
        </div>
    </aside>
<?php endif; ?>