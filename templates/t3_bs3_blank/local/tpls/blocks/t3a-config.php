<?php

if(!defined('_T3ADMIN_LOCAL_PATH')) {
    define('_T3ADMIN_LOCAL_PATH', T3_TEMPLATE_PATH . "/local/");
}

if(!defined('_T3ADMIN_CSS')) {
    define('_T3ADMIN_CSS', _T3ADMIN_LOCAL_PATH . "css/themes/t3-admin/");
}

if(!defined('_T3ADMIN_JS')) {
    define('_T3ADMIN_JS', _T3ADMIN_LOCAL_PATH . "js/");
}