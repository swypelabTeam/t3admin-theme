<?php
$user = JFactory::getUser();
?>
<?php if (!$user->guest) { ?>
    <header class='t3a-header-toolbar row'>
        <div class='hidden col-xs-12 col-sm-3 visible-xs visible-sm'>
            <div class='pull-left'>
                <i class='fa fa-bars toggle-menu visible-xs visible-sm'></i>
            </div>
        </div>
        <div class='hidden-xs col-sm-9 col-md-12'>
            <div class="t3a-profile pull-right row">
                <div class=''>
                    <i class='fa fa-user'></i>
                </div>
                <div class=''>
                    <?php echo sprintf(JText::_("Ciao, %s!"), $user->name) ?>
                </div>
                <div class='toggle-user-menu'>
                    <i class='fa fa-chevron-down'></i>
                </div>
                <nav>
                    <ul>
                        <li>
                            <a href='<?php echo JRoute::_("index.php?option=com_users&view=login&layout=logout&task=user.menulogout") ?>'><i class='fa fa-sign-out fa-fw'></i> <?php echo JText::_("Esci") ?></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
<?php } ?>