<?php
/**
 * ------------------------------------------------------------------------------
 * @package       SwypeAD for T3 Framework for Joomla!
 * ------------------------------------------------------------------------------
 * @copyright     Copyright (C) 2006 SwypeLab SRL. All Rights Reserved.
 * @authors       SwypeLab
 * @Link:         http://www.swypelab.com 
 * ------------------------------------------------------------------------------
 */
defined('_JEXEC') or die;

$this->loadBlock('t3a-config');

$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$pageclass = '';

if (is_object($menu)) {
    $pageclass = $menu->params->get('pageclass_sfx');
}
?><!DOCTYPE html>
<html class='<jdoc:include type="pageclass" />'>

    <head>
    <jdoc:include type="head" />
    <?php $this->loadBlock('t3a-head') ?>
</head>

<body class="t3-admin <?php echo (JFactory::getUser()->guest ? "guest-user" : "logged-user") ?> <?php echo $pageclass ? htmlspecialchars($pageclass) : 'default'; ?> clearfix">

    <?php $this->loadBlock('t3a-sidebar') ?>

    <div class='<?php echo (JFactory::getUser()->guest ? "col-md-12" : "col-md-9 col-lg-10" ) ?> col-sm-12 col-xs-12 t3a-wrapper'>
        <?php $this->loadBlock('t3a-toolbar') ?>
        <div class='t3a-main'>
            <jdoc:include type="component" />
            <?php if ($this->hasMessage()) : ?>
                <div class='modal message fade' tabindex="-1" role='dialog' id='system-message-modal'>
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <jdoc:include type="message" />
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</body>

</html>