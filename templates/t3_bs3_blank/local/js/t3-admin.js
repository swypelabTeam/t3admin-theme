jQuery(function ($) {

    $.fn.extend({
        isVisible: function () {
            return !!($(".toggle-menu").width() && $(".toggle-menu").height());
        }
    });

    window.isMobile = $(".toggle-menu").isVisible();

    $(document).on('click', '.toggle-menu', function () {
        $("body").toggleClass('menu-opened');
    });

    $(".t3a-show-password").each(function () {
        $(this).bind("click", function () {
            var sel = $(this).data("target");
            if (!$(sel))
                return false;

            if ($(this).hasClass("visible")) {
                $(sel).attr("type", "password");
            } else {
                $(sel).attr("type", "text");
            }
            $(this).toggleClass("visible");
        });
    });

    $("input[type='checkbox']").each(function () {
        var __input = $(this);
        var __iname = $(this).attr("name");
        __input.addClass("hidden");

        var __check = $("<i class='t3a-checkbox fa fa-circle-o'/>");
        if (__input.is(":checked")) {
            __check.toggleClass("fa-check-circle-o").toggleClass("fa-circle-o");
        }
        __check.bind("click", function () {
            if (__input.is(":checked")) {
                __input.attr("checked", "");
                __input.removeAttr("checked");
                __input.checked = false;
            } else {
                __input.attr("checked", "checked");
                __input.checked = true;
            }
            __check.toggleClass("fa-check-circle-o").toggleClass("fa-circle-o");
        });
        if ($("label[for='" + __iname + "']")) {
            $("label[for='" + __iname + "']").on("click", function () {
                __check.trigger("click");
            });
        }
        __input.after(__check);
    });

    $(document).on("click", ".t3a-profile", function () {
        $(this).toggleClass("opened");
        $(this).find("nav").stop().slideToggle();
    });

    $(window).load(function () {
        if (!window.isMobile) {
            init_sidebar();
        }
        init_toolbar();
    });

    $(window).resize(function () {
        if (!window.isMobile) {
            init_sidebar();
        }
        init_toolbar();
    });

    function init_toolbar() {
        $(".t3a-header-toolbar-placeholder").remove();
        $(".t3a-toolbar-placeholder").remove();

        $(".t3a-header-toolbar").each(function () {
            $(this).removeClass("affix").removeAttr("style");
            $(this).after("<div class='t3a-header-toolbar-placeholder'></div>");
            $(".t3a-header-toolbar-placeholder").height($(this).outerHeight(true));
            $(this).css({
                "top": $(this).position().top,
                "width": function () {
                    return  window.isMobile ? $(window).width() : $(this).width();
                }
            }).addClass("affix");
        });

        $(".t3a-toolbar").each(function () {
            $(this).removeClass("affix").removeAttr("style");
            $(this).after("<div class='t3a-toolbar-placeholder'></div>");
            $(".t3a-toolbar-placeholder").height($(this).outerHeight(true));
            $(this).css({
                "top": $(this).position().top,
                "width": function () {
                    return  window.isMobile ? $(window).width() : $(this).width();
                }
            }).addClass("affix");
        });
    }

    function init_sidebar() {
        $(".t3a-aside-placeholder").remove();
        $(".t3a-aside .t3a-sidebar-wrapper").each(function () {
            $(this).removeClass("affix").removeAttr("style");
            $(this).after("<div class='t3a-aside-placeholder'></div>");
            var width = $(".t3a-aside-placeholder").outerWidth();
            $(".t3a-aside-placeholder").height($(this).outerHeight(true));

            $(this).css({
                "top": $(this).position().top,
                "left": $(this).position().left,
                "width": width
            }).addClass("affix");
        });
    }

    if (window.Joomla) {
        Joomla = window.Joomla;
        Joomla.renderMessages = function (c) {
            var a = Joomla, b = document;
            a.removeMessages();
            var e, f, g, h, i, j, k, l, d = b.getElementById("system-message-container");
            for (e in c)
                if (c.hasOwnProperty(e)) {
                    f = c[e], g = b.createElement("div"), l = "notice" == e ? "alert-info" : "alert-" + e, l = "message" == e ? "alert-success" : l, g.className = "alert " + l;
                    var m = b.createElement("button");
                    for (m.setAttribute("type", "button"), m.setAttribute("data-dismiss", "modal"), m.className = "close", m.innerHTML = "×", g.appendChild(m), h = a.JText._(e), "undefined" != typeof h && (i = b.createElement("h4"), i.className = "alert-heading", i.innerHTML = a.JText._(e), g.appendChild(i)), j = f.length - 1; j >= 0; j--)
                        k = b.createElement("div"), k.innerHTML = f[j], g.appendChild(k);
                    d.appendChild(g);
                    $('#system-message-modal').modal();
                }
        };
        if ($('#system-message-modal').size() && $('#system-message-modal').find("#system-message").size()) {
            $('#system-message-modal').modal("show");
            $(document).on("click", "#system-message-modal .close", function () {
                $('#system-message-modal').modal("hide");
            });
        }
    }
});

