<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
?>
<div class="login<?php echo $this->pageclass_sfx; ?>">
    <?php if ($this->params->get('show_page_heading')) : ?>
        <div class="page-header">
            <h1>
                <?php echo $this->escape($this->params->get('page_heading')); ?>
            </h1>
        </div>
    <?php endif; ?>

    <?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
        <div class="login-description">
        <?php endif; ?>

        <?php if ($this->params->get('logindescription_show') == 1) : ?>
            <?php echo $this->params->get('login_description'); ?>
        <?php endif; ?>

        <?php if (($this->params->get('login_image') != '')) : ?>
            <img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JText::_('COM_USERS_LOGIN_IMAGE_ALT') ?>"/>
        <?php endif; ?>

        <?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
        </div>
    <?php endif; ?>

    <form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="form-validate form-horizontal well">

        <fieldset>
            <div class="control-group has-icon">
                <input type="text" name="username" id="username" value="" class="validate-username required has-icon-left" size="25" required="required" aria-required="true" autofocus="" aria-invalid="true">
                <i class='fa fa-user control-group-icon'></i>
            </div>
            <div class="control-group has-icon">
                <input type="password" name="password" id="password" value="" class="validate-password required has-icon-right has-icon-left" size="25" maxlength="99" required="required" aria-required="true" aria-invalid="true">
                <i class='fa fa-lock control-group-icon icon-left'></i>
                <i class='fa fa-eye control-group-icon icon-right t3a-show-password' data-target='#password'></i>
            </div>

            <?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
                <div  class="control-group">
                    <span class='pull-right'>
                        <input id="remember" type="checkbox" name="remember" class="inputbox" value="yes" checked='checked'/>
                        <label for='remember'><?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME') ?></label>
                    </span>
                </div>
            <?php endif; ?>

            <div class="control-group center">
                <button type="submit" class="btn btn-primary">
                    <?php echo JText::_('JLOGIN'); ?>
                </button>
            </div>

            <?php if ($this->params->get('login_redirect_url')) : ?>
                <input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />
            <?php else : ?>
                <input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_menuitem', $this->form->getValue('return'))); ?>" />
            <?php endif; ?>
            <?php echo JHtml::_('form.token'); ?>
        </fieldset>
    </form>
</div>
<div>
    <ul class="nav nav-tabs nav-stacked">
        <li>
            <a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
                <?php echo JText::_('COM_USERS_LOGIN_RESET'); ?>
            </a>
        </li>
        <li>
            <a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
                <?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?>
            </a>
        </li>
        <?php
        $usersConfig = JComponentHelper::getParams('com_users');
        if ($usersConfig->get('allowUserRegistration')) :
            ?>
            <li>
                <a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
                    <?php echo JText::_('COM_USERS_LOGIN_REGISTER'); ?></a>
            </li>
        <?php endif; ?>
    </ul>
</div>
